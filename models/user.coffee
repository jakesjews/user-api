mongoose = require('mongoose')
Schema   = mongoose.Schema

UserSchema = new Schema
  firstName: String
  lastName:  String
  pictures:  [String]
  profile:   {}
  location:
    type:        { type: String, default: 'Point' }
    coordinates: [Number]

UserSchema.index(location: '2dsphere')

module.exports = mongoose.model('User', UserSchema)
