FormData            = require('form-data')
fs                  = require('fs')
{expect, co, sinon} = require('../specHelper')
User                = require('../../models/user')
imageRepo           = require('../../repositories/imageRepo')
subject             = require('../../repositories/userRepo')

describe "UserRepo", ->
  describe "#create", ->
    beforeEach ->
      @context =
        request:
          body:
            firstName: 'hello'
            lastName:  'world'
            profile:
              randField: 'testing'
            location:
              coordinates: [50, 50]

       @createFunc = subject.create.bind(@context)

    it "creates a user", ->
      co =>
        yield(@createFunc())

        user = yield User.findOne(firstName: 'hello').exec()

        expect(user.lastName).to.equal('world')
        expect(user.profile.randField).to.equal('testing')
        expect(user.location.type).to.equal('Point')
        expect(user.location.coordinates).to.have.members([50,50])

    it "throws an error if the user is missing coordinates", ->
       co =>
        delete @context.request.body.location

        err = undefined

        try
          yield(@createFunc())
        catch e
          err = e

        expect(err).to.exist

  describe "#update", ->
    it "updates a user by id", ->
      co ->
        user = yield User.create
          firstName: 'testing'
          location:
            coordinates: [50, 50]

        context =
          params:
            id: user.id
          request:
            body:
              firstName: 'derp'

        yield subject.update.bind(context)()

        foundUser = yield User.findById(user.id).exec()

        expect(foundUser.firstName).to.equal('derp')

  describe "#get", ->
    it "returns a user by id", ->
      co ->
        user = yield User.create
          firstName: 'testing'
          location:
            coordinates: [50, 50]

        context =
          params:
            id: user.id

        result = yield subject.get.bind(context)()

        expect(result.firstName).to.equal('testing')
        
  describe "#getAll", ->
    it "returns all users if there are no coordinates given", ->
      co ->
        yield [
          User.create(
            firstName: 'derp'
            location:
              coordinates: [50, 50]
          ),
          User.create(
            firstName: 'herp'
            location:
              coordinates: [1, 90]
          )
        ]

        context =
          query: {}

        result = yield subject.getAll.bind(context)()

        expect(result).to.have.length(2)

    it "returns user within 5 miles of the given coordinates if there is no distance specified", ->
      co ->
        yield [
          User.create(
            firstName: 'foo'
            location:
              coordinates: [50, 50.01]
          ),
          User.create(
            firstName: 'bar'
            location:
              coordinates: [1, 90]
          )
        ]

        context =
          query:
            lng: 50
            lat: 50

        result = yield subject.getAll.bind(context)()

        expect(result).to.have.length(1)
        expect(result[0].firstName).to.equal('foo')

    it "returns user within a given distance of coordinates", ->
      co ->
        yield [
          User.create(
            firstName: 'foo'
            location:
              coordinates: [50, 50.01]
          ),
          User.create(
            firstName: 'bar'
            location:
              coordinates: [50, 51]
          ),
          User.create(
            firstName: 'bar'
            location:
              coordinates: [1, 2]
          )
        ]

        context =
          query:
            lng: 50
            lat: 50
            distance: 500000

        result = yield subject.getAll.bind(context)()

        expect(result).to.have.length(2)

  describe "#delete", ->
    it "deletes a user", ->
      co ->
        user = yield User.create
          firstName: 'foo'
          location:
            coordinates: [50, 50.01]

        context =
          params:
            id: user.id

        yield subject.delete.bind(context)()

        found = yield User.findById(user.id).exec()

        expect(found).to.not.exist

