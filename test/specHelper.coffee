mongoose  = require('mongoose')
Grid      = require('gridfs-stream')
chai      = require("chai")
sinon     = require("sinon")
sinonChai = require('sinon-chai')
co        = require('co')
dbUri     = 'mongodb://localhost/test'

require('mocha-mongoose')(dbUri)

chai.use(sinonChai)

beforeEach (done) ->
  if mongoose.connection.db
    done()
  else
    mongoose.connect(dbUri, done)
    Grid.mongo = mongoose.mongo

module.exports =
  expect: chai.expect
  co:     co
  sinon:  sinon

