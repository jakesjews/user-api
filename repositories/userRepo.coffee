User     = require('../models/user')
mongoose = require('mongoose')
Grid     = require('gridfs-stream')

module.exports =
  getAll: ->
    query = {}

    if @query.lng? && @query.lat?
      lng = parseFloat(@query.lng)
      lat = parseFloat(@query.lat)

      # default max distance is 5 miles in meters
      maxDistance = @query.distance ? 8046.72

      query =
        location:
          $near:
            $geometry:
              type: "Point"
              coordinates: [ lng, lat ]
            $maxDistance:  maxDistance

    @body = yield User.find(query).exec()

  get: ->
    @body = yield User.findById(@params.id).exec()

  create: ->
    @body = yield User.create(@request.body)

  update: ->
    @body = yield User.findOneAndUpdate({ _id: @params.id }, @request.body).exec()

  delete: ->
    user = yield User.findOneAndRemove({ _id: @params.id }).exec()
    grid = Grid(mongoose.connection.db)

    deletePicturePromises = user.pictures.map (picId) ->
      imageId = mongoose.Types.ObjectId(picId)

      new Promise (resolve, reject) ->
        grid.remove { _id: picId }, (err) ->
          if err?
            reject(err)
          else
            resolve()

    yield Promise.all(deletePicturePromises)
    @body = @params.id

