User     = require('../models/user')
parse    = require('co-busboy')
mongoose = require('mongoose')
Grid     = require('gridfs-stream')
mime     = require('mime')

module.exports =
  uploadImage: ->
    grid    = Grid(mongoose.connection.db)
    parts   = parse(this, autoFields: true)
    fileIds = []

    while part = yield parts
      fileId      = new mongoose.Types.ObjectId

      writeStream = grid.createWriteStream
        _id:          fileId
        mode:         'w'
        filename:     part.filename
        content_type: mime.lookup(part.filename)

      part.pipe(writeStream)
      fileIds.push(fileId)

    yield User.update(
      { _id: @params.id },
      { $push: pictures: $each: fileIds }
    ).exec()

    @body = { pictures: fileIds }

  getImage: (next) ->
    grid    = Grid(mongoose.connection.db)
    imageId = mongoose.Types.ObjectId(@params.id)

    file = yield new Promise (resolve, reject) =>
      grid.files.findOne { _id: imageId }, (err, file) =>
        if err?
          reject(err)
        else
          resolve(file)

    if !file?
      @throw 404
    else
      @type = file.contentType
      @body = grid.createReadStream(_id : @params.id)

