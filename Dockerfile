FROM phusion/baseimage:latest

ENV HOME /root
ENV PYTHONUNBUFFERED 1

ADD . /api

RUN curl -o iojs.tar.xz https://iojs.org/dist/v1.1.0/iojs-v1.1.0-linux-x64.tar.xz && \
    tar xf iojs.tar.xz && \
    cp -R iojs-v1.1.0-linux-x64/* /usr && \
    /usr/bin/npm install -g coffee-script && \
    rm -f iojs.tar.xz && \
    rm -rf iojs-v1.1.0-linux-x64 && \ 
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 && \
    echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list && \
    apt-get update && \
    apt-get install -y mongodb-org && \
    cd /api && /usr/bin/npm install && \
    mkdir /etc/service/mongod && \
    mkdir /etc/service/api && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD setup/mongod.sh /etc/service/mongod/run
ADD setup/api.sh /etc/service/api/run

RUN chmod +x /etc/service/mongod/run && \
    chmod +x /etc/service/api/run


EXPOSE 3000
CMD ["/sbin/my_init"]
