koa       = require('koa')
router    = require('koa-router')
logger    = require('koa-logger')
serve     = require('koa-static')
koaBody   = require('koa-body')()
mongoose  = require('mongoose')
Grid      = require('gridfs-stream')
path      = require('path')
userRepo  = require('./repositories/userRepo')
imageRepo = require('./repositories/imageRepo')

app = koa()

app
  .use(serve(__dirname + '/public'))
  .use(logger())
  .use(router(app))

app
  .get('/users',     userRepo.getAll)
  .get('/users/:id', userRepo.get)
  .post('/users',    koaBody, userRepo.create)
  .put('/users/:id', koaBody, userRepo.update)
  .del('/users/:id', userRepo.delete)
  .post('/users/:id/images/', imageRepo.uploadImage)
  .get('/images/:id', imageRepo.getImage)

Grid.mongo = mongoose.mongo
mongoose.connect('mongodb://localhost/users')

app.listen(3000)
